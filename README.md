# oks2ptree
A simple library for converting from OKS config databases to Boost's ptrees.

## The Rundown
* oks2ptree::OksParser is the main interface you should be using.
* OksParser::read_db() reads an entire database and parses it into a ptree.
* OksParser::read_class() reads all objects of a certain class in a database,
and parses them into a ptree.

## The Format
The ptrees follow this form:
```
<class> [
    <object_uid> [
        Data [
            <attribute> : <value>
            <attribute> : <value>
            <attribute> : <value>
            ...
            <multivalue_attribute> [
                0 : <value>
                1 : <value>
                2 : <value>
                ...
            ]
        ]
        Relationships [
            <relationship> : "<uid>@<class_name>"
            <multivalue_relationship> [
               0 : "<class_name>.<uid>"
               1 : "<class_name>.<uid>"
               2 : "<class_name>.<uid>"
               ...
            ]
            ...
        ]
    ]
    ...
]
```

* If no attributes or relationships are found, the `Data` or `Relationships` keys aren't created, respectively.

## Todo
- [x] Implement multivalue parsing
- [x] Implement relationship parsing
- [x] Add better error handling