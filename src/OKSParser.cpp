#include "oks2ptree/OKSParser.h"

namespace oks2ptree{
ptree OksParser::parse_object(const ConfigObject& _object) {
	ptree res;
	
	// Get class descriptor (aka schema)
	std::unique_ptr<daq::config::class_t> info;
	try {
		auto config = _object.get_configuration();
		info = std::make_unique<daq::config::class_t>(config->get_class_info(_object.class_name()));
	} catch (const daq::config::NotFound& ex) {
		throw std::runtime_error("Could not find descriptor for class " + _object.class_name());
	} catch (const daq::config::Generic& ex) {
		throw std::runtime_error("Error :" + std::string(ex.what()));
	}

	if (info->p_attributes.size()) {
		auto& data_pt = res.put_child("Data", ptree{});
		for (const auto& attribute : info->p_attributes) {
			parse_attribute(data_pt, _object, attribute);
		}
	}

	if (info->p_relationships.size()) {
		auto& relationships_pt = res.put_child("Relationships", ptree{});
		for (const auto& relationship : info->p_relationships) {
			parse_relationship(relationships_pt, _object, relationship);
		}
	}
	return res;
}

void OksParser::parse_relationship(ptree& _pt, const ConfigObject& _object, const daq::config::relationship_t& _relationship) {
	// Again, const_cast here required due to transparent changes made in ConfigObject::get()
	auto& mut_object = const_cast<ConfigObject&>(_object);
	// Multiple values for this relationship?
	if (_relationship.p_cardinality == daq::config::zero_or_many || _relationship.p_cardinality == daq::config::one_or_many) {
		std::vector<ConfigObject> rel_objects;
		try {
			mut_object.get(_relationship.p_name, rel_objects);
		} catch (const daq::config::Exception& ex) {
			throw std::runtime_error("Could not get relationship " + _relationship.p_name + " from object " + _object.full_name());
		}
		for (std::size_t i{ 0 }; i < rel_objects.size(); i++) {
			std::string rel_object_path{ rel_objects[i].class_name() + "." + rel_objects[i].UID() };
			_pt.put(_relationship.p_name + "." + std::to_string(i), rel_object_path);
		}
	} else {
		ConfigObject rel_object;
		try {
			mut_object.get(_relationship.p_name, rel_object);
		} catch (const daq::config::Exception& ex) {
			throw std::runtime_error("Could not get relationship " + _relationship.p_name + " from object " + _object.full_name());
		}
		std::string rel_object_path{ rel_object.class_name() + "." + rel_object.UID() };
		_pt.put(_relationship.p_name, rel_object_path);
	}
}

void OksParser::parse_attribute(ptree& _pt, const ConfigObject& _object, const daq::config::attribute_t& _attribute) {
	using namespace daq::config;
	switch (_attribute.p_type) {
		case bool_type:
			add_value<bool>(_pt, _object, _attribute);
			break;
		case s8_type:
			add_value<int8_t>(_pt, _object, _attribute);
			break;
		case u8_type:
			add_value<uint8_t>(_pt, _object, _attribute);
			break;
		case s16_type:
			add_value<int16_t>(_pt, _object, _attribute);
			break;
		case u16_type:
			add_value<uint16_t>(_pt, _object, _attribute);
			break;
		case s32_type:
			add_value<int32_t>(_pt, _object, _attribute);
			break;
		case u32_type:
			add_value<uint32_t>(_pt, _object, _attribute);
			break;
		case s64_type:
			add_value<int64_t>(_pt, _object, _attribute);
			break;
		case u64_type:
			add_value<uint64_t>(_pt, _object, _attribute);
			break;
		case float_type:
			add_value<float>(_pt, _object, _attribute);
			break;
		case double_type:
			add_value<double>(_pt, _object, _attribute);
			break;
		case date_type:
		case time_type:
		case enum_type:
		case class_type:
		case string_type:
			add_value<std::string>(_pt, _object, _attribute);
			break;
		default: 
			throw std::runtime_error("Invalid type on attribute " + _attribute.p_name);
		}
}

template <class T>
void OksParser::add_value(ptree& _pt, const ConfigObject& _object, const daq::config::attribute_t& _attribute) {
	// ConfigObject::get() makes changes to internal structures (cache), transparent to us.
	// As such, using const_cast here seems fitting - no visible change to _object.
	auto& mut_object = const_cast<ConfigObject&>(_object);
	if (!_attribute.p_is_multi_value) {
		T val;
		try {
			mut_object.get(_attribute.p_name, val);
		} catch (const daq::config::Exception& ex) {
			throw std::runtime_error("Could not get attribute " + _attribute.p_name + " from object " + _object.full_name());
		}
		_pt.put(_attribute.p_name, val);
		return;
	}
	std::vector<T> values;
	try {
		mut_object.get(_attribute.p_name, values);
	} catch (const daq::config::Exception& ex) {
		throw std::runtime_error("Could not get attribute " + _attribute.p_name + " from object " + _object.full_name());
	}
	for (std::size_t i{ 0 }; i < values.size(); i++) {
		_pt.put(_attribute.p_name + "." + std::to_string(i), values.at(i));
	}
}

void OksParser::read_class(const std::string& _db, ptree& _pt, const std::string& _classname) {
	// Create connection to database given by _classname
	std::unique_ptr<Configuration> config;
	try {
		config = std::make_unique<Configuration>(_db);
	} catch (const daq::config::Generic& ex) {
		throw std::runtime_error("Could not open DB: " + std::string(ex.what()));
	}

	// Read all objects of type _classname (including derived class instances)
	std::vector<ConfigObject> objects;
	try {
		config->get(_classname, objects);
	} catch (const daq::config::NotFound& ex) {
		throw std::runtime_error("Could not find class " + _classname + " in database " + _db);
	} catch (const daq::config::Generic& ex) {
		throw std::runtime_error("Error: " + std::string(ex.what()));
	}

	// Add all objects to class ptree
	_pt.clear();
	for (auto& obj : objects) {
		_pt.put_child(obj.UID(), parse_object(obj));
	}
}

void OksParser::read_db(const std::string& _db, ptree& _pt) {
	// Create connection to database given by _classname
	std::unique_ptr<Configuration> config;
	try {
		config = std::make_unique<Configuration>(_db);
	} catch (const daq::config::Generic& ex) {
		throw std::runtime_error("Could not open DB: " + std::string(ex.what()));
	}

	// Get all classes
	std::set<const std::string*> classes;
	for (const auto& c : config->superclasses()) {
		classes.insert(c.first);
	}

	_pt.clear();
	std::vector<ConfigObject> objects;
	for (const auto classname : classes) {
		try {
			config->get(*classname, objects);
		} catch (const daq::config::NotFound& ex) {
			throw std::runtime_error("Could not find class " + *classname + " in database " + _db);
		} catch (const daq::config::Generic& ex) {
			throw std::runtime_error("Error: " + std::string(ex.what()));
		}

		// Add all objects to class ptree
		for (auto& obj : objects) {
			_pt.put_child(*classname + "." + obj.UID(), parse_object(obj));
		}
		objects.clear();
	}
}

} // namespace oks2ptree