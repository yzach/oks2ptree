#ifndef OKS2PTREE_OKSPARSER_H
#define OKS2PTREE_OKSPARSER_H

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <boost/property_tree/ptree.hpp>

#include <config/Configuration.h>
#include <config/Schema.h>

namespace oks2ptree {
using boost::property_tree::ptree;
class OksParser {
private:
	// Parses _object into a a ptree
	static ptree parse_object(const ConfigObject& _object);

	// Parses the relationship from _object, puts it in _pt
	static void parse_relationship(ptree& _pt, const ConfigObject& _object, const daq::config::relationship_t& _relationship);

	// Dispatcher function; calls the correct add_value template according to type given in _attribute.p_type
	static void parse_attribute(ptree& _pt, const ConfigObject& _object, const daq::config::attribute_t& _attribute);

	// Gets the attribute's value from _object, puts it in _pt
	template <class T>
	static void add_value(ptree& _pt, const ConfigObject& _object, const daq::config::attribute_t& _attribute);
public:
	OksParser() = delete;

	// Reads all objects of class _classname found in _db into _pt.
	// Clears _pt.
	static void read_class(const std::string& _db, ptree& _pt, const std::string& _classname);

	// Reads the entire db given by _db into _pt.
	// Clear _pt.
	static void read_db(const std::string& _db, ptree& _pt);
};
} // namespace oks2ptree

#endif // OKS2PTREE_OKSPARSER_H