#include <iostream>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <oks2ptree/OKSParser.h>

using boost::property_tree::ptree;

int main(int argc, char** argv) {
	const std::string path{ "oksconfig:oks2ptree/test/test.data.xml" };
	const std::string out_path{ "oks2ptree/test/test_out.json" };

	try {
		std::cout << "[+] Attempting to read OKS data from " << path << std::endl;
		std::cout << "[+] Reading all objects of all classes" << std::endl;
		ptree pt;
		oks2ptree::OksParser::read_db(path, pt);
		/*
		oks2ptree::OksParser::read_class(path, pt, "Department");
		std::cout << "[+] Reading all objects of class \"Department\"" << std::endl;
		for (const auto& child : pt.get_child("Department.Data")) {
			std::cout << "\t[+] UID: " << child.first << std::endl;
			for (const auto& x : child.second) {
				std::cout << "\t\t[+] " << x.first << " = " << x.second.get_value<std::string>() << std::endl;
			}
		}
		*/
		std::cout << "[+] Dumping ptree to file " << out_path << std::endl;
		boost::property_tree::json_parser::write_json(out_path, pt);
	} catch (const std::exception& e) {
		std::cout << "[!] " << e.what() << std::endl;
	}
}
